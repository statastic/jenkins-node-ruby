FROM registry.gitlab.com/statastic/jenkins-node:2.1.0

ENV NOKOGIRI_USE_SYSTEM_LIBRARIES=1

ADD gemrc /root/.gemrc

USER root

RUN \
apt-get update \
&& apt-get install -y --no-install-recommends \
build-essential \
ruby \
ruby-dev \
ruby-curb \
ruby-zip \
ruby-zip-zip \
ruby-influxdb \
ruby-httpclient \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/* \
&& gem install minitest-junit \
&& gem install roo \
&& apt-get clean autoclean

USER jenkins
